////////////////////////////////////
/////// Coder: Coder Mario Alberto Moreno Contreras
////////////////////////////////////
module sdp_sc_ram #( 
parameter  Data_Length      = 32,
parameter  ADDRESS_width    = 4,
parameter ADDRESS_index = $clog2(ADDRESS_width)
)
(
// Core clock a
input                         clk      ,
// Memory interface
input                         we       ,   // Write enable
input       [Data_Length-1:0]      data     ,   // data to be stored
output   [(Data_Length-1):0]      rd_data  ,   // read data from memory
input       [ADDRESS_index-1:0]      wr_addr  ,   // Read write address
input       [ADDRESS_index-1:0]      rd_addr      // Read write address
);

// Memory depth
localparam  W_DEPTH     = ADDRESS_index;

// Declare a RAM variable 
reg [Data_Length-1:0]  ram [8:0];

always@(posedge clk) begin
if(we)
    ram[wr_addr] <= data;

end
assign rd_data =ram[rd_addr];
endmodule
