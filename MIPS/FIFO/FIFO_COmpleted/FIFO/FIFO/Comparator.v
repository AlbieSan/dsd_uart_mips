module Comparator
#(
	parameter ADDRESS_width = 4,
	parameter COUNTER_SIZE = $clog2(ADDRESS_width)
)
(
	input [COUNTER_SIZE:0] add_push,
	input [COUNTER_SIZE:0] add_pop,
	output  full,
	output empty
);

assign full = ((add_push[COUNTER_SIZE] != add_pop[COUNTER_SIZE]) && (add_push[COUNTER_SIZE-1:0] == add_pop[COUNTER_SIZE-1:0]))? 1'b1:1'b0;
assign empty = (add_push == add_pop)? 1'b1: 1'b0;
endmodule
