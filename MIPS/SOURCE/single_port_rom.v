module single_port_rom
#(parameter DATA_WIDTH=32, parameter ADDR_WIDTH=5)
(
	input [(ADDR_WIDTH-1):0] addr,
	output reg [(DATA_WIDTH-1):0] q
);


	reg [DATA_WIDTH-1:0] rom[2**ADDR_WIDTH-1:0];

	initial
	begin
		$readmemb("C:/Users/Jaime/Documents/MIPS/SOURCE/instructions_memory.txt", rom);
	end

	always@(*)
	begin
		q = rom[addr];
	end

endmodule
