//coder: Jaime Hernandez
module Control
(
	//SIGNALS to the CU
	input clk,
	input reset,
	input [5:0] Opcode,
	input [5:0] funct,
	input [15:0]I_type_constant,
	input [4:0] PC_current,
	//output signals CU
	output reg PC_en,
	output reg [3:0] ALU_control,
	output reg [1:0] ALUSrcA,
	output reg [1:0] ALUSrcB,
	output reg Write_enable,
	output reg IRWrite,
	output reg Write_address_mux,
	output reg IorD,
	output reg MemWrite,
	output reg MemtoReg,
	output reg Tx_Start
);

//States definitions
localparam IFECTH = 0;
localparam DECODE = 1;
localparam ACT = 2;
localparam MA_LOAD = 3;
localparam MAWB_STORE = 4;
localparam EXECUTE = 5;
localparam WB_LOAD = 6;
localparam WB = 7;
//*****
reg [3:0]State;
//FSM core logic
always@(posedge clk or negedge reset) begin
	 if (!reset)
		State <= IFECTH;
	else 
		case(State)
			IFECTH:
				State <= DECODE;	
			DECODE:
				 if (Opcode == 6'h00) //R-type intruction go to Register Arthimetic operation
					State <= EXECUTE;
				else 
					State <= ACT;	//I-type goes to Address calculation						
			ACT: begin
				if (Opcode == 6'h08 | Opcode == 6'h0C	| Opcode == 6'h0D)	//addi and andi and ori is treated as a R-type
					State <= WB;
				else if(Opcode == 6'h23) //lw
					State <= MA_LOAD;
				else if(Opcode == 6'h2B)	//sw
					State <= MAWB_STORE;
				else 
					State <= IFECTH;
				end
			MA_LOAD:
				State <= WB_LOAD;
			MAWB_STORE:			//
				State <= IFECTH;
			EXECUTE:
				State <= WB;
			
			WB_LOAD:
				State <=IFECTH;
			WB:
				State <= IFECTH;
				
			default:
				State <= IFECTH;
	endcase
end

//FSM output logic
always@(State) begin
	PC_en 		= 0; //enable PC reg
	ALU_control	= 0;//Sum
	Write_enable = 0;
	ALUSrcB	= 1;
	ALUSrcA = 1;
	IRWrite = 0;
	Write_address_mux =0;
	IorD=0;
	MemWrite=0;
	MemtoReg=0;
	Tx_Start=0;
	
		case(State)
			IFECTH: begin		    //INITIAL STATE REFERENCE PAGE 9 
				if(PC_current < 5'h16) begin
					PC_en 		= 1; //enable PC reg
					ALU_control	= 0; //Sum
					IRWrite		= 1;
					ALUSrcA		= 0;	//keep Rs as input to ALU A
					end
				else 
					PC_en 		= 0; //enable PC reg
			end
			DECODE: begin
				PC_en 		= 0; 
				ALU_control	= 0; //Sum
				end
			ACT: begin
				ALUSrcB	= 2;	// immediate input to ALU B
				ALUSrcA	= 1;	//Rs input to ALU A
				if(Opcode == 6'h0C) //IFECTH andi opcocer
					ALU_control = 1;
				else 
					ALU_control = 0;	//Sum as ALU op default

				if (I_type_constant == 16'hffff)
					Tx_Start = 1;
				else 
					Tx_Start = 0;
				end
			MA_LOAD:
				IorD	= 1;
			MAWB_STORE:	begin
				IorD	= 1;
				MemWrite =1;
				end
			EXECUTE: begin
				ALUSrcB	=	2'b00;	// Rt B input to ALU
				if(funct == 6'h00 | funct == 6'h02 ) begin	// Shifts funct- set ALU op control and shamt to ALU for sll/srl
					ALUSrcA		=	2'b10;	// Shamt instr[10:6] B input to ALU
					ALU_control =	3; //shift left
					end
				else if(funct == 6'h25)//OR
					ALU_control = 2;
				else
					ALU_control = 0;
				end
			WB_LOAD: begin
				MemtoReg = 1;
				Write_enable 	= 1;
				end
			WB: begin
				Write_enable 	= 1;
				ALUSrcB			= 2;
					if(Opcode == 0) //IFECTH R-type
						Write_address_mux = 1; //Rd (instr[15:11]) as Write Address
					else 
						Write_address_mux = 0;	//Rt (instr[20:16]) as Write Address
				
				end
		endcase
end

endmodule
