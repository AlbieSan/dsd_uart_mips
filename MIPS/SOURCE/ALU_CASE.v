//(Coder: Jaime HDZ
module ALU_CASE
#(	parameter WORD_LENGTH=32
)

(

	input [3:0]Control,
	input [WORD_LENGTH -1 : 0] A,
	input [WORD_LENGTH -1 : 0] B,
	

	output [WORD_LENGTH - 1 : 0] C_output,
	output Carry_output

);
reg [WORD_LENGTH -1 : 0] C;
reg Carry;
	

always@(Control, A, B) begin
	case (Control)	
		4'b0000: {Carry, C} = A + B;						
		4'b0001: {Carry, C} = A & B;	
		4'b0010: {Carry, C} = A | B;	
		4'b0011: {Carry, C} = B << A;	
		4'b0100: {Carry, C} = B >> A;	
		default: {Carry, C} = 7'b0000000;
		endcase
		
end		
		
		assign C_output 		= C;
		assign Carry_output	= Carry;


endmodule
