//// Coder:MArio MOreno
//// Mux 3 to 1 A=1(Stop-Bit, B=0(START_BIT), C= Serial DATA)

module Mux_3_to_1(
input A,
input B,
input C,
input D,
input [1:0] sel,
output reg Out
);

//reg out_reg;
always@(sel or A or B or C or D) begin
	case(sel)
		2'b00: Out=A;
		2'b01: Out=B;
		2'b10: Out=C;
		2'b11: Out=D;
	default: Out=A;
	endcase
end
endmodule
