//creator:;Mario Moreno
 module pipo
#(
	parameter WORD_LENGTH = 8
)

(
	// Input Ports
	input clk,
	input reset,
	input [WORD_LENGTH-1 : 0] Data_Input,
	input enable,

	// Output Ports
	output [WORD_LENGTH-1 : 0] Data_Output
);

reg  [WORD_LENGTH-1 : 0] Data_reg;

always@(posedge clk or negedge reset) begin
	if(reset==1'b0)
		Data_reg <=  {WORD_LENGTH{1'd0}};
	else if(enable==1'b1)
		Data_reg <= Data_Input;

end

assign Data_Output = Data_reg;

endmodule
