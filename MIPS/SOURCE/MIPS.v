/////// coder: Mario Moreno
//////// File: MIPS top
module MIPS
#(
	parameter WORD_LENGTH = 32, 
	parameter DATA_ITEMS = 32,
	parameter BAUD_RATE= 115200,
	parameter UARTCLK=10_000_000,
	parameter ADDR_WIDTH = CeilLog2(DATA_ITEMS),
	parameter I_typing = DATA_ITEMS/2
	
)
(
	//input singlas
	input f_clk,
	input reset,
	input clk_uart,
	
	//output
	output [WORD_LENGTH-1:0]instr,
	output Tx_Serial_data_out
	
);
/***** COntrol_wire ******/
//wire [WORD_LENGTH-1:0]instr;
wire Write_enable;
wire PC_en;
wire IorD_wire;
wire MemWrite;
wire IRWrite;
wire Write_address_mux;
wire MemtoReg;
wire [1:0]ALUSrcA; //¿No debe ser de un solo bit? Consultar el libro
wire [1:0]ALUSrcB;
wire [3:0]ALUControl;
/***** DATA_wire ******/
/***** ALU_wires ******/
wire [WORD_LENGTH-1:0] ALU_out_wire;
wire [WORD_LENGTH-1:0] ALU_regout_wire;
wire [WORD_LENGTH-1:0] ALU_PC_wire;
wire [WORD_LENGTH-1:0]SRCA_wire;
wire [WORD_LENGTH-1:0]SRCB_wire;

/***** IM_wires ******/
wire [WORD_LENGTH-1:0] imem_out_wire;

/***** PC_wires ******/
wire [WORD_LENGTH-1:0] PC_current;

/***** RF_wires ******/
	/***** Input_wires ******/
wire [ADDR_WIDTH-1:0]Waddr_wire;
wire [WORD_LENGTH-1:0]Wdata_wire;
	/***** output_wires ******/ 
wire [WORD_LENGTH-1:0]read_data_2;
wire [WORD_LENGTH-1:0]read_data_1;

/***** Instreg_wires ******/
//wire [WORD_LENGTH-1:0]instr;

/***** DATAreg_wires ******/
wire [WORD_LENGTH-1:0]Dtreg_out_wire;

/***** Sign_extend ******/
wire [WORD_LENGTH-1:0]Sign_extended;

wire clk;
//wire clk_uart;

/*UART wire*****/



/***********************************  Program_Counter   *************************/
pipo #(
	.WORD_LENGTH(WORD_LENGTH)
)
Program_Counter
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(ALU_out_wire),
	.enable(PC_en),

	// Output Ports
	.Data_Output(PC_current)
);


/**************PLL*************
PLL_F	PLL_inst (
	.inclk0 ( f_clk ),
	.c0 ( clk_uart ),	//UART 10 Mhz clk 
	.c1 ( clk )	///MIPS 500 KHz clk
	);
*/
	
	assign clk = f_clk;
/***********************************  MIPS_Control_Unit   *************************/
Control
Control_unit
(
	//SIGNALS to the CU
	.clk(clk),
	.reset(reset),
	.Opcode(instr[31:26]),
	.funct(instr[5:0]),
	.I_type_constant(instr[15:0]),
	.PC_current(PC_current),
	//output signals CU
	.PC_en(PC_en),
	.ALU_control(ALUControl),//to alu
	.ALUSrcA(ALUSrcA),//To ALU MUXA
	.ALUSrcB(ALUSrcB),
	.Write_enable(Write_enable),
	.IRWrite(IRWrite),//to Instr_reg entity's enable signal
	.Write_address_mux(Write_address_mux),
	.IorD(IorD_wire),
	.MemWrite(MemWrite),///to instruction memory
	.MemtoReg(MemtoReg),
	.Tx_Start(Tx_Start)//habilitar UART
);

/***************** MUX_ALU_PC********************/
Mux2_unit #(
	.WORD_LENGTH(WORD_LENGTH)
)
Mux_IorD
(
	.In_0(PC_current), 
	.In_1(ALU_regout_wire),
	.En(IorD_wire),
	.Out(ALU_PC_wire)
); 

/***************** INSTR/DATAMEMORY  ********************/
intructions_memory
#(
 .DATA_WIDTH(WORD_LENGTH),
 .ADDR_WIDTH(ADDR_WIDTH)
)
INSTR_DATAMEMORY
(
	// Input Ports
	.clk(clk),
	.we(MemWrite),//from control
	.IorD(IorD_wire),//from control
	.addr(ALU_PC_wire), //from MUX_ALU_PC
	.data(read_data_2), //FROM RF2
	
	// Output Ports
	.mem_output(imem_out_wire)
	
);
/***************** Instruction_reg ********************/
pipo #(
	.WORD_LENGTH(WORD_LENGTH)
)
Instr_reg
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(imem_out_wire),
	.enable(IRWrite),//from control signal IRWrite

	// Output Ports
	.Data_Output(instr)
);
/***************** DATA_reg ********************/
pipo #(
	.WORD_LENGTH(WORD_LENGTH)
)
DATA_reg
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(imem_out_wire),
	.enable(1'b1),//señal no definida en control. Le pondre un uno?

	// Output Ports
	.Data_Output(Dtreg_out_wire)
);



/***************** MUX_WRaDDRESS_RFile********************/
Mux2_unit #(
	.WORD_LENGTH(ADDR_WIDTH)//MUX DE ANCHO DE PALABRA DE 5BITS
)
Mux_writeaDDREESRFile
(
	.In_0(instr[20:16]), 
	.In_1(instr[15:11]),//ESTEMUX,NO ES DE 32
	.En(Write_address_mux),
	.Out(Waddr_wire)
); 
/***************** MUX_WRite DATA_RFile********************/
Mux2_unit #(
	.WORD_LENGTH(WORD_LENGTH)
)
Mux_writedata
(
	.In_0(ALU_regout_wire), 
	.In_1(Dtreg_out_wire),
	.En(MemtoReg),//mEMTOREG
	.Out(Wdata_wire)
); 
/***************************  Register_File   *******************/
Register_File #(
	.Reg_Address($clog2(DATA_ITEMS)),
	.Data_Length(WORD_LENGTH),
	.Reg_Length(WORD_LENGTH)
	
)
RegisterFile
(
	.clk(clk),
	.reset(reset),
	.enable(Write_enable), /// from control
	.WriteRegister(Waddr_wire),
	.ReadRegister1(instr[25:21]),
	.ReadRegister2(instr[20:16]),
	.WriteData(Wdata_wire),
	.ReadData1(read_data_1),
	.ReadData2(read_data_2)
);
/***************************  Sign_extend   *******************/
sign_ext_unit
#(
	.Data_lenght(WORD_LENGTH),
	.Half_lenght(I_typing)

)
sign_extend
(
 .data_in_16b(instr[15:0]),
 .data_out_32b(Sign_extended)//this oputput to mux
 );
 
 /***************************  ALU_MUXA   *******************/
 //This is ALU 2_to_1
 Mux3_unit #(
	.WORD_LENGTH(WORD_LENGTH)
)
SRCA_MUX
(
	.In_0(PC_current), 
	.In_1(read_data_1),
	.In_2({27'b0,instr[10:6]}),
	.En(ALUSrcA),
	.Out(SRCA_wire)
);     

 /***************************  ALU_MUXB   *******************/
   //This is ALU 4_to_1 or 3_to_1
Mux4_unit#(
	.WORD_LENGTH(WORD_LENGTH),
	.Selec_width(2)
)
SRCB_MUX
(
.In_0(read_data_2),
.In_1(32'b1),
.In_2(Sign_extended),
.In_3(32'b0),//Acorde a Pizano debe ser HighZ
.Enable(ALUSrcB),
.Out(SRCB_wire)
 );

  /***************************  ALU  *******************/
ALU_CASE
#(	
  .WORD_LENGTH(WORD_LENGTH)
)
ALU_unit
(

	.Control(ALUControl),
	.A(SRCA_wire),
	.B(SRCB_wire),
	

	.C_output(ALU_out_wire)
	//output Carry_output no necesitamos el carry. Corroborar en el libro. No recuerdo haber leído la relación del  carry

);
  /***************************  REG  *******************/
  /*This a pipo*/
pipo
#(	
  .WORD_LENGTH(WORD_LENGTH)
)
ALU_REG
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(ALU_out_wire),
	.enable(1'b1),

	// Output Ports
	.Data_Output(ALU_regout_wire)
);

//**********************************************///
//***********    UART  ************************///
UART
#(
	.PLL_CLK(10_000_000),
	.BAUD_RATE(115200)

)
UART_Tx_inst
(
		//Input_Ports
	.clk(clk_uart),
	.reset(reset),
	.Tx_Start(Tx_Start),
	.Tx_word(read_data_2),
	
	//Output Ports
	.Tx_Serial_data_out(Tx_Serial_data_out),
	.Tx_done_word(Tx_done_word)
);


 /*********************************************************************************************/
   
 /*Log Function*/
     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i < data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

/*********************************************************************************************/



endmodule
