/////// coder: MArio Moreno
/////// piso: tx data input from FPGA. TX serial data out from piso
module piso_msb #(
parameter DW = 8,
parameter Frame_lenght = 9
) (
input               clk,    // Clock
input               rst,    // asynchronous reset low active 
input               enb,    // Enable
input               l_s,    // load or shift
input  [DW-1:0]     inp,    // data input
output              out,     // Serial output,
output 				  parity
);

reg  [DW-1:0]      rgstr_r, rgst_r_nxt;
reg ff_en;

assign parity = ~^inp;

always@(*)
begin
    ff_en = enb || l_s;
	 if(enb)
		rgst_r_nxt =  {rgstr_r[0], rgstr_r[DW-1:1]};
	 else
	 begin
		if(l_s)
			rgst_r_nxt = inp;
	end

end

always@(posedge clk or negedge rst) 
begin
    if(!rst)
        rgstr_r  <= {DW{1'd0}};
    else
		if (ff_en)
        rgstr_r  <= rgst_r_nxt;
end

assign out  = rgstr_r[0];    // MSB bit is the first to leave
endmodule
