onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Control_Unit
add wave -noupdate /MIPS_TB/DUV/Control_unit/clk
add wave -noupdate /MIPS_TB/DUV/Control_unit/reset
add wave -noupdate -color Coral /MIPS_TB/DUV/Control_unit/Opcode
add wave -noupdate /MIPS_TB/DUV/Control_unit/funct
add wave -noupdate /MIPS_TB/DUV/Control_unit/I_type_constant
add wave -noupdate -color {Medium Orchid} -radix unsigned /MIPS_TB/DUV/Control_unit/PC_current
add wave -noupdate /MIPS_TB/DUV/Control_unit/PC_en
add wave -noupdate /MIPS_TB/DUV/Control_unit/ALU_control
add wave -noupdate /MIPS_TB/DUV/Control_unit/ALUSrcA
add wave -noupdate /MIPS_TB/DUV/Control_unit/ALUSrcB
add wave -noupdate /MIPS_TB/DUV/Control_unit/Write_enable
add wave -noupdate /MIPS_TB/DUV/Control_unit/IRWrite
add wave -noupdate /MIPS_TB/DUV/Control_unit/Write_address_mux
add wave -noupdate /MIPS_TB/DUV/Control_unit/IorD
add wave -noupdate /MIPS_TB/DUV/Control_unit/MemWrite
add wave -noupdate /MIPS_TB/DUV/Control_unit/MemtoReg
add wave -noupdate /MIPS_TB/DUV/Control_unit/Tx_Start
add wave -noupdate -radix unsigned /MIPS_TB/DUV/Control_unit/State
add wave -noupdate -divider Register_File
add wave -noupdate /MIPS_TB/DUV/RegisterFile/clk
add wave -noupdate /MIPS_TB/DUV/RegisterFile/reset
add wave -noupdate /MIPS_TB/DUV/RegisterFile/enable
add wave -noupdate -color Brown -radix unsigned /MIPS_TB/DUV/RegisterFile/WriteRegister
add wave -noupdate -color {Orange Red} -radix hexadecimal /MIPS_TB/DUV/RegisterFile/WriteData
add wave -noupdate -color Tan -radix unsigned /MIPS_TB/DUV/RegisterFile/ReadRegister1
add wave -noupdate -radix unsigned /MIPS_TB/DUV/RegisterFile/ReadRegister2
add wave -noupdate -color {Orange Red} -radix unsigned /MIPS_TB/DUV/RegisterFile/ReadData1
add wave -noupdate -radix unsigned /MIPS_TB/DUV/RegisterFile/ReadData2
add wave -noupdate -radix unsigned /MIPS_TB/DUV/RegisterFile/data_reg_1
add wave -noupdate -radix unsigned /MIPS_TB/DUV/RegisterFile/data_reg_2
add wave -noupdate /MIPS_TB/DUV/RegisterFile/i
add wave -noupdate -divider PC
add wave -noupdate /MIPS_TB/DUV/Program_Counter/clk
add wave -noupdate /MIPS_TB/DUV/Program_Counter/reset
add wave -noupdate /MIPS_TB/DUV/Program_Counter/Data_Input
add wave -noupdate /MIPS_TB/DUV/Program_Counter/enable
add wave -noupdate /MIPS_TB/DUV/Program_Counter/Data_Output
add wave -noupdate /MIPS_TB/DUV/Program_Counter/Data_reg
add wave -noupdate -divider Memory_INSTR
add wave -noupdate /MIPS_TB/DUV/INSTR_DATAMEMORY/clk
add wave -noupdate -color {Light Blue} -radix unsigned /MIPS_TB/DUV/INSTR_DATAMEMORY/addr
add wave -noupdate -radix unsigned /MIPS_TB/DUV/INSTR_DATAMEMORY/data
add wave -noupdate -color Turquoise /MIPS_TB/DUV/INSTR_DATAMEMORY/we
add wave -noupdate -color Blue /MIPS_TB/DUV/INSTR_DATAMEMORY/IorD
add wave -noupdate -radix hexadecimal /MIPS_TB/DUV/INSTR_DATAMEMORY/mem_output
add wave -noupdate /MIPS_TB/DUV/INSTR_DATAMEMORY/rom_output
add wave -noupdate -radix hexadecimal /MIPS_TB/DUV/INSTR_DATAMEMORY/ram_output
add wave -noupdate -divider MUXA
add wave -noupdate -radix unsigned /MIPS_TB/DUV/SRCA_MUX/In_0
add wave -noupdate -radix unsigned /MIPS_TB/DUV/SRCA_MUX/In_1
add wave -noupdate -radix unsigned /MIPS_TB/DUV/SRCA_MUX/In_2
add wave -noupdate /MIPS_TB/DUV/SRCA_MUX/En
add wave -noupdate -radix unsigned /MIPS_TB/DUV/SRCA_MUX/Out
add wave -noupdate /MIPS_TB/DUV/SRCA_MUX/Out_reg
add wave -noupdate -divider MUXB
add wave -noupdate -radix unsigned /MIPS_TB/DUV/SRCB_MUX/In_0
add wave -noupdate -radix unsigned /MIPS_TB/DUV/SRCB_MUX/In_1
add wave -noupdate -radix unsigned /MIPS_TB/DUV/SRCB_MUX/In_2
add wave -noupdate -radix unsigned /MIPS_TB/DUV/SRCB_MUX/In_3
add wave -noupdate /MIPS_TB/DUV/SRCB_MUX/Enable
add wave -noupdate -radix unsigned /MIPS_TB/DUV/SRCB_MUX/Out
add wave -noupdate -radix unsigned /MIPS_TB/DUV/SRCB_MUX/Out_reg
add wave -noupdate -divider ALU
add wave -noupdate /MIPS_TB/DUV/ALU_unit/Control
add wave -noupdate -radix unsigned /MIPS_TB/DUV/ALU_unit/A
add wave -noupdate -radix unsigned /MIPS_TB/DUV/ALU_unit/B
add wave -noupdate -radix unsigned /MIPS_TB/DUV/ALU_unit/C_output
add wave -noupdate /MIPS_TB/DUV/ALU_unit/Carry_output
add wave -noupdate -radix unsigned /MIPS_TB/DUV/ALU_unit/C
add wave -noupdate -divider ALU_REG
add wave -noupdate /MIPS_TB/DUV/ALU_REG/clk
add wave -noupdate /MIPS_TB/DUV/ALU_REG/reset
add wave -noupdate /MIPS_TB/DUV/ALU_REG/Data_Input
add wave -noupdate /MIPS_TB/DUV/ALU_REG/enable
add wave -noupdate -radix unsigned /MIPS_TB/DUV/ALU_REG/Data_Output
add wave -noupdate -radix unsigned /MIPS_TB/DUV/ALU_REG/Data_reg
add wave -noupdate -color Magenta -radix unsigned /MIPS_TB/DUV/Mux_writeaDDREESRFile/In_0
add wave -noupdate -color {Cornflower Blue} -radix unsigned /MIPS_TB/DUV/Mux_writeaDDREESRFile/In_1
add wave -noupdate /MIPS_TB/DUV/Mux_writeaDDREESRFile/En
add wave -noupdate -radix unsigned /MIPS_TB/DUV/Mux_writeaDDREESRFile/Out
add wave -noupdate -divider MUX_PC
add wave -noupdate -radix unsigned /MIPS_TB/DUV/Mux_IorD/In_0
add wave -noupdate -radix unsigned /MIPS_TB/DUV/Mux_IorD/In_1
add wave -noupdate /MIPS_TB/DUV/Mux_IorD/En
add wave -noupdate /MIPS_TB/DUV/Mux_IorD/Out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {5560 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 309
configure wave -valuecolwidth 304
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {5481 ns} {5995 ns}
