//coder MarioMoreno
module sipo #(
parameter DW = 8
) (
input               clk,
input               rst,
input               enb,
input               inp,
output  [DW-1:0]    out
);

reg  [DW-1:0]      rgstr_r;

always@(posedge clk or negedge rst) begin: rgstr_label
    if(!rst)
        rgstr_r  <= {DW{1'd0}};
    else if (enb)
        rgstr_r  <= {rgstr_r[DW-2:0], inp};
end

assign out  = rgstr_r;

endmodule
