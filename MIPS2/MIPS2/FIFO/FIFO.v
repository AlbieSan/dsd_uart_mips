//////////
///  Coder Mario Alberto Moreno Contreras
/// FIFO 
module FIFO
#(
 parameter Data_Length   = 32,
 parameter ADDRESS_width = 9,
 parameter ADDRESS_index = $clog2(ADDRESS_width) 
)
(
/*************** Inputs *********************/
 input [Data_Length-1:0] Data_Input,
 input pop,
 input push,
 input reset,
 input clk,
 output [Data_Length-1:0] Data_Output,
 output full,
 output empty 
);
endmodule
