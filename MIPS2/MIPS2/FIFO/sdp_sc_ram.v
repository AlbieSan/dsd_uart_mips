////////////////////////////////////
/////// Coder: Coder Mario Alberto Moreno Contreras
////////////////////////////////////
module sdp_sc_ram #( 
parameter  Data_Length      = 32,
parameter  ADDRESS_width    = 9
)
(
// Core clock a
input                         clk      ,
// Memory interface
input                         we       ,   // Write enable
input       [Data_Length-1:0]      data     ,   // data to be stored
output reg  [Data_Length-1:0]      rd_data  ,   // read data from memory
input       [ADDRESS_width-1:0]      wr_addr  ,   // Read write address
input       [ADDRESS_width-1:0]      rd_addr      // Read write address
);

// Memory depth
localparam  W_DEPTH     = 2**W_ADDR;

// Declare a RAM variable 
reg [Data_Length-1:0]  ram [W_DEPTH-1:0];

always@(posedge clk) begin
if(we)
    ram[wr_addr] <= data;
rd_data <= ram [rd_addr];
end

endmodule
