module POP_COUNTER
#(
	parameter ADDRESS_width = 4,
	parameter COUNTER_SIZE = $clog2(ADDRESS_width)
)
(
	input pop,
	input reset,
	input empty,
	input clk,
	
	output  [COUNTER_SIZE:0] add_pop
);

reg [COUNTER_SIZE:0] add_pop_beta;

always@(posedge clk or negedge reset) begin
	if(reset == 0)
		add_pop_beta = {ADDRESS_width{1'b0}};
	else begin
	if(pop && ~empty)
	  add_pop_beta = add_pop_beta + 1'b1;
	
	end
end
assign add_pop = add_pop_beta;
endmodule
