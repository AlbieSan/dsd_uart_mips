module FIFO_TB;

 parameter Data_Length   = 32;
 parameter ADDRESS_width = 4;
 parameter ADDRESS_index = $clog2(ADDRESS_width);
 
 
 // Input Ports
reg tb_fpga_clk = 0;
reg tb_reset = 1;
reg tb_pop=0;
reg tb_push=0;
reg [Data_Length-1:0] tb_Data_input = 8'h00;

// Output Ports
wire tb_full;
wire tb_empty;
wire [Data_Length-1:0] tb_Data_output;

/********************* Device Under Verification **************/
FIFO

#(
	// Parameter Declarations
  .Data_Length(Data_Length),
  .ADDRESS_width(ADDRESS_width),
  .ADDRESS_index(ADDRESS_index)
)
DUV
(
/*************** Inputs *********************/
 .Data_Input(tb_Data_input),
 .pop(tb_pop),
 .push(tb_push),
 .reset(tb_reset),
 .clk(tb_fpga_clk),
 /*************** Outputs *********************/
 .Data_Output(tb_Data_output),
 .full(tb_full),
 .empty(tb_empty)
);



/**************************************************************************/
	
/******************** Stimulus *************************/
initial // Clock generator
  begin
    forever #2 tb_fpga_clk = !tb_fpga_clk;
  end
/*----------------------------------------------------------------------------------------*/
initial begin /*Reset*/
	# 0 tb_reset = 1'b0;
	#3 tb_reset = 1'b1;
end
/*----------------------------------------------------------------------------------------*/
initial begin 
	# 88 tb_pop  = 0;
	# 4 tb_pop  = 1;
	# 4 tb_pop  = 0;
	# 4 tb_pop  = 1;
	# 4 tb_pop  = 0;
	# 4 tb_pop  = 1;
	# 4 tb_pop  = 0;
	# 4 tb_pop  = 1;
	# 4 tb_pop  = 0;
	# 4 tb_pop  = 1;
	# 4 tb_pop  = 0;
	# 4 tb_pop  = 1;
	# 4 tb_pop  = 0;
	# 4 tb_pop  = 1;
	# 4 tb_pop  = 0;
	# 4 tb_pop  = 1;
	# 4 tb_pop  = 0;
	# 4 tb_pop  = 0;
end 
/*----------------------------------------------------------------------------------------*/
initial begin 
	# 10  tb_push = 0;
	# 4  tb_push = 1;
	# 4  tb_push = 0;
	# 4  tb_push = 1;
	# 4  tb_push = 0;
	# 4  tb_push = 1;
	# 4  tb_push = 0;
	# 4  tb_push = 1;
	# 4  tb_push = 0;
	# 4  tb_push = 1;	
	# 4  tb_push = 0;
	# 4  tb_push = 1;
	# 4  tb_push = 0;
	# 4  tb_push = 1;
	# 4  tb_push = 0;
	# 4  tb_push = 1;
	# 4  tb_push = 0;
	# 4  tb_push = 0;	
end
/*----------------------------------------------------------------------------------------*/ 
initial begin
	# 2  tb_Data_input = 1;
	# 12  tb_Data_input = 8;
	# 6  tb_Data_input = 7;
	# 6  tb_Data_input = 6;
	# 6  tb_Data_input = 5;
	# 6  tb_Data_input = 0;
	# 6  tb_Data_input = 4;
	# 6  tb_Data_input = 0;
	# 6  tb_Data_input = 3;
	# 6  tb_Data_input = 0;
	# 6  tb_Data_input = 2;
	# 6  tb_Data_input = 0;
	# 6  tb_Data_input = 1;
end


/*--------------------------------------------------------------------*/



endmodule
 
 

 