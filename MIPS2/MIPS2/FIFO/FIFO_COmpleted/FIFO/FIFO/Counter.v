// coder: Mario Alberto Moreno
module Counter
#(
	// Parameter Declarations
	parameter N_BITS = 4
)

(
	// Input Ports
	input clk,
	input reset,
	input enable,
	
	// Output Ports
	output[N_BITS - 1:0] counter

);

reg [N_BITS-1 : 0] counter_r;

	always@(posedge clk or negedge reset) begin
		if (reset == 1'b0)
			counter_r <= {N_BITS{1'b0}};
		else begin
			if(enable == 1'b1)
					counter_r <= counter_r + 1'b1;
			end
		end
	
assign counter = counter_r;

endmodule
