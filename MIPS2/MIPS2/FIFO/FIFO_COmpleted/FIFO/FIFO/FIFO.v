//////////
///  Coder Mario Alberto Moreno Contreras
///  FIFO 
module FIFO
#(
 parameter Data_Length   = 32,
 parameter ADDRESS_width = 4,
 parameter ADDRESS_index = $clog2(ADDRESS_width) 
)
(
/*************** Inputs *********************/
 input [Data_Length-1:0] Data_Input,
 input pop,
 input push,
 input reset,
 input clk,
 /*************** Outputs *********************/
 output [Data_Length-1:0] Data_Output,
 output full,
 output empty 
);
wire we_wire;
wire wire_full;
wire wire_empty;
wire [ADDRESS_index:0]address_write;
wire [ADDRESS_index:0]address_read;
wire [Data_Length-1 : 0] w_output_data_RAM;

assign we_wire = push & (~wire_full);
/************ PUSH debouncer ***********/
//ONE_SHOT PUSH_SHOT
//(
///******* INPUTS ************/
//	.clk(clk),
//	.reset(reset),
//	.load_botton(push),
///****** OUTPUTS ***********/
//	.shot(push_wire)
//);
///*****************************/
///************ POP debouncer **********/
//ONE_SHOT POP_SHOT
//(
///******* INPUTS ************/
//	.clk(clk),
//	.reset(reset),
//	.load_botton(pop),
///****** OUTPUTS ***********/
//	.shot(pull_wire)
//);
PUSH_COUNTER
#(
	.ADDRESS_width(ADDRESS_width)
)
Pushing
(
	.push(push),
	.clk(clk),
	.reset(reset),
	.full(full),
	
	.add_push(address_write)
);
POP_COUNTER
#(
	.ADDRESS_width(ADDRESS_width)
)
POPIS
(
	.pop(pop),
	.reset(reset),
	.empty(empty),
	.clk(clk),
	
	.add_pop(address_read)
);
Comparator
#(
	.ADDRESS_width (ADDRESS_width)
	
)
Comp
(
	.add_push(address_write),
	.add_pop(address_read),
	.full(wire_full),
	.empty(wire_empty)
);
sdp_sc_ram
#( 
.Data_Length(Data_Length),
.ADDRESS_width(ADDRESS_width)
)
RAM
(
// Core clock a
	.clk(clk),
// Memory interface
	.we(we_wire),   // Write enable
   .data(Data_Input),   // data to be stored
   .rd_data(Data_Output),   // read data from memory
   .wr_addr(address_write[ADDRESS_index-1:0]) ,   // Read write address
   .rd_addr(address_read[ADDRESS_index-1:0])      // Read write address
	
		// Output Ports
	//.q(w_output_data_RAM)
);

//assign o_Data_output = w_output_data_RAM;
assign full= wire_full;
assign empty =wire_empty;
endmodule
