// Coder: MArio MOreno
module Mux4_unit#(
	parameter WORD_LENGTH = 32,
	parameter Selec_width = 2
)
(input [WORD_LENGTH-1:0]  In_0,
 input [WORD_LENGTH-1:0]  In_1,
 input [WORD_LENGTH-1:0]  In_2,
 input [WORD_LENGTH-1:0]  In_3,
 input [Selec_width-1:0]  Enable,
 output [WORD_LENGTH-1:0] Out
 );
reg [WORD_LENGTH-1:0] Out_reg;
always@(*)begin
 case(Enable)
  2'b00:   Out_reg = In_0;
  2'b01:   Out_reg = In_1;
  2'b10:   Out_reg = In_2;
  2'b11:   Out_reg = In_3;
  default: Out_reg = In_0;
 endcase
end
assign Out = Out_reg;
endmodule 