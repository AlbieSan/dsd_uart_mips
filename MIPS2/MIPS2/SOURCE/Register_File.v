//Coder:          Mario Moreno
//Date:           11/04/2019
//Name:           Register_File.sv
//Modified by: Mario Moreno
//Date:        30/11/2019
//Description:    This is a Register File with for and generate
module Register_File #(
	parameter Reg_Address = 5,
	parameter Data_Length = 32,
	parameter Reg_Length  = 32
	
)
(
	input clk,
	input reset,
	input enable,
	input [Reg_Address-1:0] WriteRegister,
	input [Reg_Address-1:0] ReadRegister1,
	input [Reg_Address-1:0] ReadRegister2,
	input [Data_Length-1:0] WriteData,
	output [Data_Length-1:0] ReadData1,
	output [Data_Length-1:0] ReadData2	
);


reg [Data_Length-1:0] Register_Bank [Reg_Length-1:0];
reg [Data_Length-1:0] data_reg_1;
reg [Data_Length-1:0] data_reg_2;
integer i;
always@(posedge clk or negedge reset) begin
	if (!reset)	begin
		// reset register_bank
		for (i=0; i<(Data_Length); i=i+1) begin
			Register_Bank[i] <= {Data_Length{1'b0}};
		end
	end
	else
	  if(enable)
        Register_Bank[WriteRegister] <= WriteData;
end
//synchronous read for mips multicycle

always@(posedge clk or negedge reset) begin
	if (!reset) begin
		data_reg_1 <= {Data_Length{1'b0}};
		data_reg_2 <= {Data_Length{1'b0}};
		end
	else begin
		data_reg_1 <= Register_Bank [ReadRegister1];
		data_reg_2 <= Register_Bank [ReadRegister2];
		end
	end
assign ReadData1 = data_reg_1;
assign ReadData2 = data_reg_2; 
endmodule 