module UART_control2
(
	//input singlas to the FSM
	input clk,
	input reset,
	input Tx_Start,
	input Tx_Done_flag,
	
	//output signals
	output reg Tx_Transmit,
	output reg [1:0] Byte_sel,
	output reg Tx_done_word

);

//States definitions
localparam IDLE = 0;
localparam TX_B0 = 1;
localparam TX_B1 = 2;
localparam TX_B2 = 3;
localparam TX_B3 = 4;
localparam READY = 5;
//*****


reg [3:0]State;

//FSM core logic
always@(posedge clk or negedge reset) begin
	if (reset==0)
		State <= IDLE;
	else 
		case(State)
			IDLE: begin
				if (Tx_Start)
					State <= TX_B0;
				else	
					State <= IDLE;
					end
			TX_B0: begin
				if (Tx_Done_flag)
					State <= TX_B1;
				else	
					State <= TX_B0;
				end	
			TX_B1: begin
				if (Tx_Done_flag)
					State <= TX_B2;
				else	
					State <= TX_B1;
				end	
			TX_B2: begin
				if (Tx_Done_flag)
					State <= TX_B3;
				else	
					State <= TX_B2;
					end
			TX_B3: begin
				if (Tx_Done_flag)
					State <= IDLE;
				else	
					State <= TX_B3;
				end
			READY: 
				State <= IDLE;
			default:
				State <= IDLE;
	endcase
end

//FSM output logic
always@(State) begin
	Tx_Transmit		= 1; 
	Byte_sel			= 0;
	Tx_done_word 	= 0;
	
		case(State)
			IDLE:
				Tx_Transmit		= 0;			
			TX_B1:
				Byte_sel	= 1;
			TX_B2:
				Byte_sel	= 2;
			TX_B3:
				Byte_sel	= 3;
			READY:
				Tx_done_word = 0;
		endcase
end

endmodule
