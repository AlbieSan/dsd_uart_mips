//Coder:          Mario Moreno
//Date:           30/11/2019
//Name:           sign_ext_unit.sv
module sign_ext_unit
#(
	parameter Data_lenght= 32,
	parameter Half_lenght= 16

)
(input [Half_lenght-1:0] data_in_16b,
output [Data_lenght-1:0] data_out_32b); 

reg [Data_lenght-1:0] data_out_32b_reg;
always@(data_in_16b)begin 
if(data_in_16b[15])
	data_out_32b_reg =  {16'b1,data_in_16b};
else
	data_out_32b_reg =  {16'b0,data_in_16b};
end 
assign data_out_32b = data_out_32b_reg;
endmodule 