
  
module UART_Tx
  #(
  parameter BAUD_RATE = 115200,
  parameter CLK		 = 10_000_000,
  parameter CLKS_PER_BIT = CLK/BAUD_RATE
  
  )
  (
  //Input ports
   input       clk,
	input			reset,
   input       Tx_Transmit,
   input [7:0] Tx_Byte_input,
	
	//output ports	
   output    	Tx_Active,
   output reg  Tx_Serial_data_out	= 1, //to start with bus at high level
   output      Tx_Done_flag
   );
  
  localparam IDLE        		= 3'b000;
  localparam Tx_Transmit_BIT	= 3'b001;
  localparam TX_DATA_BITS		= 3'b010;
  localparam TX_PARTITY_BIT	= 3'b011;
  localparam TX_STOP_BIT		= 3'b100;
  localparam CLEANUP				= 3'b101;
   
  reg [2:0]    State;
  reg [7:0]    clk_Counter;
  reg [3:0]    reg_Bit_Index;
  reg [7:0]    reg_Tx_Data;
  reg          reg_Tx_Done_flag	=0;
  reg          reg_Tx_Active 		=0;
     
  always @(posedge clk or negedge reset) begin
	if (!reset)
		State <= IDLE;
	else begin
		case (State)
        IDLE : begin
            Tx_Serial_data_out   <= 1'b1;     // line in high in idle state
            reg_Tx_Done_flag     <= 1'b0;
            clk_Counter <= 0;
            reg_Bit_Index   <= 0;
             
            if (Tx_Transmit == 1'b1) begin
                reg_Tx_Active <= 1'b1;
                reg_Tx_Data   <= Tx_Byte_input;
                State   <= Tx_Transmit_BIT;
              end
            else
              State <= IDLE;
          end 
         
         
        // Send  Start Bit. Start bit = 0
        Tx_Transmit_BIT : begin
            Tx_Serial_data_out <= 1'b0;
             
            // Wait CLKS_PER_BIT-1 clock cycles for start bit to finish
            if (clk_Counter < CLKS_PER_BIT-1) begin
                clk_Counter <= clk_Counter + 1'b1;
                State     <= Tx_Transmit_BIT;
              end
            else begin
                clk_Counter <= 0;
                State     <= TX_DATA_BITS;
              end
				 
          end 
			 
         
        // Wait CLKS_PER_BIT-1 clock cycles for data bits to finish         
        TX_DATA_BITS : begin
            Tx_Serial_data_out <= reg_Tx_Data[reg_Bit_Index];
             
            if (clk_Counter < CLKS_PER_BIT-1) begin
                clk_Counter <= clk_Counter + 1'b1;
                State     <= TX_DATA_BITS;
              end
            else begin
                clk_Counter <= 0;
                 
                // Check if we have sent out all bits
                if (reg_Bit_Index < 7) begin
                    reg_Bit_Index <= reg_Bit_Index + 1'b1;
                    State   <= TX_DATA_BITS;
                  end
                else begin
                    reg_Bit_Index <= 0;
                    State   <= TX_PARTITY_BIT;
                  end
              end
          end 
         
			
			//TO send Parity bit
			TX_PARTITY_BIT: begin
            
				Tx_Serial_data_out <= ~^reg_Tx_Data;	//high when even
				
            // Wait CLKS_PER_BIT-1 clock cycles for parity bit
            if (clk_Counter < CLKS_PER_BIT-1) begin
                clk_Counter <= clk_Counter + 1'b1;
                State     <= TX_PARTITY_BIT;
              end
            else begin
				   State				<= TX_STOP_BIT;
               clk_Counter		<= 0;
              end
          end 
           
        // To send Stop bit
        TX_STOP_BIT : begin
            Tx_Serial_data_out <= 1'b1;
             
            // Wait CLKS_PER_BIT-1 clock cycles for Stop bit to finish
            if (clk_Counter < CLKS_PER_BIT-1) begin
                clk_Counter <= clk_Counter + 1'b1;
                State     <= TX_STOP_BIT;
              end
            else begin
                reg_Tx_Done_flag     <= 1'b1;
                clk_Counter <= 0;
                State     <= CLEANUP;
                reg_Tx_Active   <= 1'b0;
              end
          end
       CLEANUP:  begin
		    State <= IDLE;
			 reg_Tx_Done_flag     <= 1'b0;
			 end
        default :
          State <= IDLE;
         
      endcase
    end
	end
 
  assign Tx_Active = reg_Tx_Active;
  assign Tx_Done_flag   = reg_Tx_Done_flag;
   
endmodule 