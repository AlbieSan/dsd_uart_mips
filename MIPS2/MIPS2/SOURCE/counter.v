//Coder: Moreno
//Module: counter 3 bit up
//description : a counter with enable and reset

module counter #(
parameter wrd_len = 4,
parameter MAXCNT  = 4'd5
)(

//inputs
input clk,
input rst,
input enb,

//outputs
output ovf
//output [wrd_len-1:0] counter
);

reg [wrd_len-1:0] temp_count;
reg [wrd_len-1:0]   cntr_next_r;
reg            ovf_r, ovf_en;

always@(*) begin: comp_and_next_val
   ovf_r       = (temp_count >= MAXCNT-1) ;
   cntr_next_r = (ovf_r)? ( {wrd_len{1'd0}} ):( temp_count +1'd1 );
   ovf_en    = ovf_r && enb ;
end

always @ (posedge clk or negedge rst) begin
	if(!rst)
		temp_count <= {wrd_len{1'd0}};
	else
		if(enb)
		temp_count <= cntr_next_r;
end
//assign counter = temp_count;
assign ovf = ovf_en;

endmodule
