//coder: MARIO MORENO
module Mux2_unit #(
	parameter WORD_LENGTH = 32
)
(input [WORD_LENGTH-1:0] In_0, 
input[WORD_LENGTH-1:0]In_1,
input En,
output [WORD_LENGTH-1:0]Out
);          

assign Out = (En)?In_1:In_0; 
endmodule
