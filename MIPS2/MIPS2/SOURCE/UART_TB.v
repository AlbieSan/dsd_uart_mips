
`timescale 1ns / 1ns

module UART_TB;

parameter PLL_CLK			= 10_000_000;
parameter BAUD_RATE 		= 115200;
parameter CLOCK_PER_BIT	= PLL_CLK/BAUD_RATE;		//87 for 10Mhz clock and 115200 Baud rate


parameter CLOCK_PERIOD	= 100; // in Nanoseconds  Period of 10 MHZ clock
parameter BIT_TIME		= 8600;	// nanosecond bit time


reg		clk_tb				= 0;
reg 		reset_tb				= 0;
reg 		Serial_data_in_tb	= 1;
reg 		clear_Rx_flag_tb	= 0;
reg      Tx_Transmit_tb		= 1;
reg[7:0] Tx_Byte_input_tb	= 0;

wire      	Rx_flag_tb;
wire [7:0]	Data_Rx_tb;
wire			Tx_Active_tb;
wire			Tx_Serial_data_out_tb;
wire      	Tx_Done_flag_tb;

UART_TL
#(
	.PLL_CLK(PLL_CLK),
	.BAUD_RATE(BAUD_RATE)
	//.CLOCK_PER_BIT(CLOCK_PER_BIT)
)
DUV
(
	//input ports
	.fpga_clk(clk_tb),
	.reset(reset_tb),
	.Serial_data_in(Tx_Serial_data_out_tb),
	.clear_Rx_flag(clear_Rx_flag_tb),
	.Tx_Transmit(Tx_Transmit_tb),
	.Tx_Byte_input(Tx_Byte_input_tb),
	
	//Output ports
	.Rx_flag(Rx_flag_tb),
	.Data_Rx(Data_Rx_tb),
	.Tx_Active(Tx_Active_tb),
	.Tx_Serial_data_out(Tx_Serial_data_out_tb),
	.Tx_Done_flag(Tx_Done_flag_tb)
);

/*********************************************************/
initial // 10 Mhz Clock generator
  begin
    forever #(CLOCK_PERIOD/2) clk_tb = !clk_tb;
  end
/*********************************************************/
initial begin // reset generator
   #0 reset_tb = 0;
   #(CLOCK_PERIOD*2) reset_tb = 1;
end
/*********************************************************/
initial begin // Tx_Transmit_tb generator
	#0 Tx_Transmit_tb = 1;
	#(CLOCK_PERIOD*50) Tx_Transmit_tb = 0;
	#(CLOCK_PERIOD*86) Tx_Transmit_tb = 1;
end
/*********************************************************/
initial begin // Serial_data_in_tb generator
	#0 Serial_data_in_tb = 1;
end
/*********************************************************/
/*********************************************************/
initial begin // clear_Rx_flag_tb generator
	#0 clear_Rx_flag_tb = 0;
	#(BIT_TIME*11) clear_Rx_flag_tb = 1;
end
/*********************************************************/
/*********************************************************/
initial begin // Tx_Byte_input_tb generator
	#(CLOCK_PERIOD*2) Tx_Byte_input_tb = 8'h81;
end
/*********************************************************/




endmodule
