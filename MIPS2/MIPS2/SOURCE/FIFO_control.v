//MArio Moren0 coder
module FIFO_control
(
	//input singlas to the FSM
	input clk,
	input reset,
	input Tx_Start,
	input Tx_Done_flag,
	input full_fifo,
	input empty_FIFO,
	input fill_null,
	input cnter_uartOVF,
	
	
	//output signals
	output muxpush,
	output fakepush,
	output pop,
	output enableuartcntr,
	output startuart
);

//States definitions
localparam IDLE = 0;
localparam FILL= 1;
localparam WAIT= 2;
localparam FILLNULL = 3;
localparam TXBYTE1=4;
localparam WAITx1= 5;
localparam TXBYTE2= 6;
localparam WAITx2= 7;
localparam TXBYTE3= 8;
localparam WAITx3= 9;
localparam TXBYTE4= 10;
localparam POP1= 11;
localparam POP2=12;
localparam POP3=13;
//*****

reg push_reg;
reg muxp_reg;
reg pop_reg;
reg en_counr;
reg startuartreg;
reg [7:0]State;

//FSM core logic
always@(posedge clk or negedge reset) begin
	if (reset==0)
		State <= IDLE;
	else 
		case(State)
			IDLE: begin
				if (Tx_Start)
					State <= FILL;
				else	
					State <= IDLE;
					end
			FILL: begin
				if (fill_null)
					State <= WAIT;
				else	
					State <= FILL;
				end	
			WAIT:
			 State <= FILLNULL;
			FILLNULL: 
				State <= TXBYTE1;
			TXBYTE1:
			begin
				if(Tx_Done_flag)
					State <= WAITx1;
				else
					State <= TXBYTE1;
			end
			WAITx1:
			begin
				if(cnter_uartOVF)
					State <= POP1;
				else
					State <= WAITx1;
			end
			POP1:
				State <= TXBYTE2;
			TXBYTE2:
							
			begin
				if(Tx_Done_flag)
					State <= WAITx2;
				else
					State <= TXBYTE2;
			end

			WAITx2:
			begin
				if(cnter_uartOVF)
					State <= POP2;
				else
					State <= WAITx2;
			end
			POP2:
				State <= TXBYTE3;
			TXBYTE3:
			begin
				if(Tx_Done_flag)
					State <= WAITx3;
				else
					State <= TXBYTE3;
			end
			
			WAITx3:
			begin
				if(cnter_uartOVF)
					State <= POP3;
				else
					State <= WAITx3;
			end
			
			POP3:
				State <= TXBYTE4;
			TXBYTE4:
			begin
				if(Tx_Done_flag)
					State <= IDLE;
				else
					State <= TXBYTE4;
			end
			
			default:
				State <= IDLE;
	endcase
end

//FSM output logic
always@(State) begin
		case(State)
			IDLE:
			begin
				push_reg		= 0;
				muxp_reg=0;
				en_counr=0;
				startuartreg=0;
				pop_reg=0;
			end
			FILL:
			begin
				push_reg		= 0;
				muxp_reg=0;
				en_counr=0;
				startuartreg=0;
				pop_reg=0;
			end
			WAIT:
			begin
				push_reg		= 0;
				muxp_reg=0;
				
				en_counr=0;
				startuartreg=0;
				pop_reg=0;
			end
			FILLNULL:
			begin
				push_reg		= 1;
				muxp_reg=1;
				en_counr=0;
				startuartreg=0;
				pop_reg=0;
			end
			TXBYTE1:
			begin
				en_counr=0;
				startuartreg=1;
				pop_reg=0;
			end
			WAITx1:
			begin
				en_counr=1;
				startuartreg=0;
				pop_reg=0;
			end
			TXBYTE2:
			begin
				en_counr=0;
				startuartreg=1;
				pop_reg=0;
			end
			
			WAITx2:
			begin
				en_counr=1;
				startuartreg=0;
				pop_reg=0;
			end
			TXBYTE3:
			begin
				en_counr=0;
				startuartreg=1;
				pop_reg=0;
			end
			WAITx3:
			begin
				en_counr=1;
				startuartreg=0;
				pop_reg=0;
			end
			TXBYTE4:
			begin
				en_counr=0;
				startuartreg=1;
				pop_reg=0;
			end
			POP1:
			begin
				en_counr=0;
				startuartreg=0;
				pop_reg=1;
			end
			POP2:
			begin
				en_counr=0;
				startuartreg=0;
				pop_reg=1;
			end
			POP3:
			begin
				en_counr=0;
				startuartreg=0;
				pop_reg=1;
			end
			
			

		endcase
end
assign muxpush=muxp_reg;
assign fakepush=push_reg;
assign pop = pop_reg;
assign enableuartcntr= en_counr;
assign startuart= startuartreg;
endmodule
