module And_Module(A,B,Out);
input A;
input B;
output Out;

assign Out = A & B;
endmodule
