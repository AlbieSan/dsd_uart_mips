/////// coder: Mario Moreno
//////// File: MIPS top
module MIPS
#(
	parameter WORD_LENGTH = 32, 
	parameter DATA_ITEMS = 32,
	parameter BAUD_RATE= 115200,
	parameter UARTCLK=10_000_000,
	parameter ADDR_WIDTH = CeilLog2(DATA_ITEMS),
	parameter I_typing = DATA_ITEMS/2
	
)
(
	//input singlas
	input f_clk,
	input reset,
	input clk_uart,
	//output
	output [WORD_LENGTH-1:0]instr,
	output Tx_Serial_data_out
	
);
/***** COntrol_wire ******/
//wire [WORD_LENGTH-1:0]instr;

wire Write_enable;
wire PC_en;
wire IorD_wire;
wire MemWrite;



//wire MemWrite;
wire IRWrite;
wire Write_address_mux/*synthesis keep*/;
wire MemtoReg/*synthesis keep*/;
wire [1:0]ALUSrcA/*synthesis keep*/; //¿No debe ser de un solo bit? Consultar el libro
wire [1:0]ALUSrcB/*synthesis keep*/;
wire [3:0]ALUControl/*synthesis keep*/;
/***** DATA_wire ******/
/***** ALU_wires ******/
wire [WORD_LENGTH-1:0] ALU_out_wire/*synthesis keep*/;
wire [WORD_LENGTH-1:0] ALU_regout_wire/*synthesis keep*/;
wire [WORD_LENGTH-1:0] ALU_PC_wire/*synthesis keep*/;
wire [WORD_LENGTH-1:0]SRCA_wire/*synthesis keep*/;
wire [WORD_LENGTH-1:0]SRCB_wire/*synthesis keep*/;

/***** IM_wires ******/
wire [WORD_LENGTH-1:0] imem_out_wire/*synthesis keep*/;

/***** PC_wires ******/
wire [WORD_LENGTH-1:0] PC_current/*synthesis keep*/;

/***** RF_wires ******/
	/***** Input_wires ******/
wire [ADDR_WIDTH-1:0]Waddr_wire/*synthesis keep*/;
wire [WORD_LENGTH-1:0]Wdata_wire/*synthesis keep*/;
	/***** output_wires ******/ 
wire [WORD_LENGTH-1:0]read_data_2/*synthesis keep*/;
wire [WORD_LENGTH-1:0]read_data_1/*synthesis keep*/;

/***** Instreg_wires ******/
//wire [WORD_LENGTH-1:0]instr;

/***** DATAreg_wires ******/
wire [WORD_LENGTH-1:0]Dtreg_out_wire/*synthesis keep*/;

/***** Sign_extend ******/
wire [WORD_LENGTH-1:0]Sign_extended/*synthesis keep*/;


//wire clk;
//wire clk_uart;
wire [7:0] FIFO_INPUT_wire;
wire [7:0] FIFO_OUTPUT_wire;
wire full_FIFO;
wire empty_FIFO;
wire ovf_wire;
wire pop_wire;
wire fakepush_wire;
wire muxpush_wire;
wire start_uart;
wire enablecntr;
wire ovf_wire1;
/*UART wire*****/




//wire uart_clk;
wire Tx_Start/*synthesis keep*/;
/***********************************  Program_Counter   *************************/
pipo #(
	.WORD_LENGTH(WORD_LENGTH)
)
Program_Counter
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(ALU_out_wire),
	.enable(PC_en),

	// Output Ports
	.Data_Output(PC_current)
);


/**************PLL**************
PLL_F	PLL_inst (
.inclk0 ( f_clk ),
	.c0 ( clk_uart ),	//UART 10 Mhz clk 
	.c1 ( clk )	///MIPS 500 KHz clk
	);

	*/
assign clk = f_clk;
/***********************************  MIPS_Control_Unit   *************************/
Control
Control_unit
(
	//SIGNALS to the CU
	.clk(clk),
	.reset(reset),
	.Opcode(instr[31:26]),
	.funct(instr[5:0]),
	.I_type_constant(instr[15:0]),
	.PC_current(PC_current),
	//output signals CU
	.PC_en(PC_en),
	.ALU_control(ALUControl),//to alu
	.ALUSrcA(ALUSrcA),//To ALU MUXA
	.ALUSrcB(ALUSrcB),
	.Write_enable(Write_enable),
	.IRWrite(IRWrite),//to Instr_reg entity's enable signal
	.Write_address_mux(Write_address_mux),
	.IorD(IorD_wire),
	.MemWrite(MemWrite),///to instruction memory
	.MemtoReg(MemtoReg),
	.Tx_Start(Tx_Start)//habilitar UART
);

/***************** MUX_ALU_PC********************/
Mux2_unit #(
	.WORD_LENGTH(WORD_LENGTH)
)
Mux_IorD
(
	.In_0(PC_current), 
	.In_1(ALU_regout_wire),
	.En(IorD_wire),
	.Out(ALU_PC_wire)
); 

/***************** INSTR/DATAMEMORY  ********************/
intructions_memory
#(
 .DATA_WIDTH(WORD_LENGTH),
 .ADDR_WIDTH(ADDR_WIDTH)
)
INSTR_DATAMEMORY
(
	// Input Ports
	.clk(clk),
	.we(MemWrite),//from control
	.IorD(IorD_wire),//from control
	.addr(ALU_PC_wire), //from MUX_ALU_PC
	.data(read_data_2), //FROM RF2
	
	// Output Ports
	.mem_output(imem_out_wire)
	
);
/***************** Instruction_reg ********************/
pipo #(
	.WORD_LENGTH(WORD_LENGTH)
)
Instr_reg
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(imem_out_wire),
	.enable(IRWrite),//from control signal IRWrite

	// Output Ports
	.Data_Output(instr)
);
/***************** DATA_reg ********************/
pipo #(
	.WORD_LENGTH(WORD_LENGTH)
)
DATA_reg
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(imem_out_wire),
	.enable(1'b1),//señal no definida en control. Le pondre un uno?

	// Output Ports
	.Data_Output(Dtreg_out_wire)
);



/***************** MUX_WRaDDRESS_RFile********************/
Mux2_unit #(
	.WORD_LENGTH(ADDR_WIDTH)//MUX DE ANCHO DE PALABRA DE 5BITS
)
Mux_writeaDDREESRFile
(
	.In_0(instr[20:16]), 
	.In_1(instr[15:11]),//ESTEMUX,NO ES DE 32
	.En(Write_address_mux),
	.Out(Waddr_wire)
); 
/***************** MUX_WRite DATA_RFile********************/
Mux2_unit #(
	.WORD_LENGTH(WORD_LENGTH)
)
Mux_writedata
(
	.In_0(ALU_regout_wire), 
	.In_1(Dtreg_out_wire),
	.En(MemtoReg),//mEMTOREG
	.Out(Wdata_wire)
); 
/***************************  Register_File   *******************/
Register_File #(
	.Reg_Address($clog2(DATA_ITEMS)),
	.Data_Length(WORD_LENGTH),
	.Reg_Length(WORD_LENGTH)
	
)
RegisterFile
(
	.clk(clk),
	.reset(reset),
	.enable(Write_enable), /// from control
	.WriteRegister(Waddr_wire),
	.ReadRegister1(instr[25:21]),
	.ReadRegister2(instr[20:16]),
	.WriteData(Wdata_wire),
	.ReadData1(read_data_1),
	.ReadData2(read_data_2)
);
/***************************  Sign_extend   *******************/
sign_ext_unit
#(
	.Data_lenght(WORD_LENGTH),
	.Half_lenght(I_typing)

)
sign_extend
(
 .data_in_16b(instr[15:0]),
 .data_out_32b(Sign_extended)//this oputput to mux
 );
 
 /***************************  ALU_MUXA   *******************/
 //This is ALU 2_to_1
 Mux3_unit #(
	.WORD_LENGTH(WORD_LENGTH)
)
SRCA_MUX
(
	.In_0(PC_current), 
	.In_1(read_data_1),
	.In_2({27'b0,instr[10:6]}),
	.En(ALUSrcA),
	.Out(SRCA_wire)
);     

 /***************************  ALU_MUXB   *******************/
   //This is ALU 4_to_1 or 3_to_1
Mux4_unit#(
	.WORD_LENGTH(WORD_LENGTH),
	.Selec_width(2)
)
SRCB_MUX
(
.In_0(read_data_2),
.In_1(32'b1),
.In_2(Sign_extended),
.In_3(32'b0),//Acorde a Pizano debe ser HighZ
.Enable(ALUSrcB),
.Out(SRCB_wire)
 );

  /***************************  ALU  *******************/
ALU_CASE
#(	
  .WORD_LENGTH(WORD_LENGTH)
)
ALU_unit
(

	.Control(ALUControl),
	.A(SRCA_wire),
	.B(SRCB_wire),
	

	.C_output(ALU_out_wire)
	//output Carry_output no necesitamos el carry. Corroborar en el libro. No recuerdo haber leído la relación del  carry

);
  /***************************  REG  *******************/
  /*This a pipo*/
pipo
#(	
  .WORD_LENGTH(WORD_LENGTH)
)
ALU_REG
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(ALU_out_wire),
	.enable(1'b1),

	// Output Ports
	.Data_Output(ALU_regout_wire)
);

//********************** FIFO ************************///
FIFO
#(
 .Data_Length(8),
 .ADDRESS_width(4)
)
FIFO_STATEMENT
(
/*************** Inputs *********************/
 .Data_Input(FIFO_INPUT_wire),
 .pop(pop_wire),
 .push(Tx_Start ||fakepush_wire),
 .reset(reset),
 .clk(clk),
 /*************** Outputs *********************/
 .Data_Output(FIFO_OUTPUT_wire),
 .full(full_FIFO),
 .empty(empty_FIFO)
);
/*****************MUX_FIFO*******************/
Mux2_unit #(
	.WORD_LENGTH(8)
)
MUX_FIFO
(
.In_0(read_data_2[7:0]), 
.In_1(8'b0),
.En(muxpush_wire),
.Out(FIFO_INPUT_wire)
); 
//////fillCounter
counter #(
.wrd_len(4),
.MAXCNT(4'd3)
)
Fill_Counter
(

//inputs
.clk(clk),
.rst(reset),
.enb(Tx_Start),

//outputs
.ovf(ovf_wire)
//output [wrd_len-1:0] counter
);

FIFO_control
FIF_cntrl
(
	//input singlas to the FSM
	.clk(clk),
	.reset(reset),
	.Tx_Start(Tx_Start),
	.Tx_Done_flag(Tx_done_word),
	.full_fifo(full_FIFO),
	.empty_FIFO(empty_FIFO),
	.fill_null(ovf_wire),
	.cnter_uartOVF(ovf_wire1),
	
	
	//output signals
	.muxpush(muxpush_wire),
	.fakepush(fakepush_wire),
	.pop(pop_wire),
	.enableuartcntr(enablecntr),
	.startuart(start_uart)
);

UART
#(
	.PLL_CLK(10_000_000),
	.BAUD_RATE(115200)

)
UART_Tx_inst
(
		//Input_Ports
	.clk(clk_uart),
	.reset(reset),
	.Tx_Start(start_uart),
	.Tx_word(FIFO_OUTPUT_wire),
	
	//Output Ports
	.Tx_Serial_data_out(Tx_Serial_data_out),
	.Tx_done_word(Tx_done_word)
);

//////fillCounter
counter #(
.wrd_len(11),
.MAXCNT(1023)
)
UART_count
(

//inputs
.clk(clk),
.rst(reset),
.enb(enablecntr),

//outputs
.ovf(ovf_wire1)
//output [wrd_len-1:0] counter
);



 /*********************************************************************************************/
   
 /*Log Function*/
     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i < data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

/*********************************************************************************************/



endmodule
