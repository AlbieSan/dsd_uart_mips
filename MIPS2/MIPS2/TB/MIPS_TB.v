`timescale 1ns / 1ns

module MIPS_TB;

parameter WORD_LENGTH = 32;
parameter DATA_ITEMS = 32;
parameter BAUD_RATE= 115200;
parameter UARTCLK=10_000_000;
parameter I_typing = DATA_ITEMS/2;
	


reg clk_tb=0;
reg clk_UART_tb=0;
reg reset_tb=1;

wire [31:0] instr;
wire Tx_Serial_data_out_tb;



MIPS
#(
	.WORD_LENGTH(WORD_LENGTH),
	.DATA_ITEMS(DATA_ITEMS),
	.BAUD_RATE(BAUD_RATE),
   .UARTCLK(UARTCLK),
   .I_typing(I_typing)

)
DUV
(
		//input singlas
	.f_clk(clk_tb),
	.reset(reset_tb),
	.clk_uart(clk_UART_tb),
	//output
	.instr(instr),
	.Tx_Serial_data_out(Tx_Serial_data_out_tb)
	
	
);

/*********************************************************/
initial // MIPS Clock generator
  begin
    forever #50 clk_tb = !clk_tb;	//5 KHz
  end
/*********************************************************/
initial begin // reset generator
   #0 reset_tb = 0;
   #3 reset_tb = 1;
end
/*********************************************************/
/*********************************************************/
initial // UART Clock generator
  begin
    forever #50 clk_UART_tb = !clk_UART_tb;	//10MHz clock
  end
/*********************************************************/



endmodule