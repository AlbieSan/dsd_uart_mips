onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /MIPS_TB/DUV/Control_unit/clk
add wave -noupdate /MIPS_TB/DUV/Control_unit/reset
add wave -noupdate /MIPS_TB/DUV/Control_unit/Opcode
add wave -noupdate /MIPS_TB/DUV/Control_unit/funct
add wave -noupdate /MIPS_TB/DUV/Control_unit/I_type_constant
add wave -noupdate /MIPS_TB/DUV/Control_unit/PC_current
add wave -noupdate /MIPS_TB/DUV/Control_unit/PC_en
add wave -noupdate /MIPS_TB/DUV/Control_unit/ALU_control
add wave -noupdate /MIPS_TB/DUV/Control_unit/ALUSrcA
add wave -noupdate /MIPS_TB/DUV/Control_unit/ALUSrcB
add wave -noupdate /MIPS_TB/DUV/Control_unit/Write_enable
add wave -noupdate /MIPS_TB/DUV/Control_unit/IRWrite
add wave -noupdate /MIPS_TB/DUV/Control_unit/Write_address_mux
add wave -noupdate /MIPS_TB/DUV/Control_unit/IorD
add wave -noupdate /MIPS_TB/DUV/Control_unit/MemWrite
add wave -noupdate /MIPS_TB/DUV/Control_unit/MemtoReg
add wave -noupdate /MIPS_TB/DUV/Control_unit/Tx_Start
add wave -noupdate /MIPS_TB/DUV/Control_unit/State
add wave -noupdate /MIPS_TB/DUV/UART_Tx_inst/clk
add wave -noupdate /MIPS_TB/DUV/UART_Tx_inst/reset
add wave -noupdate /MIPS_TB/DUV/UART_Tx_inst/Tx_Start
add wave -noupdate /MIPS_TB/DUV/UART_Tx_inst/Tx_word
add wave -noupdate /MIPS_TB/DUV/UART_Tx_inst/Tx_Active
add wave -noupdate /MIPS_TB/DUV/UART_Tx_inst/Tx_Serial_data_out
add wave -noupdate /MIPS_TB/DUV/UART_Tx_inst/Tx_done_word
add wave -noupdate /MIPS_TB/DUV/UART_Tx_inst/Byte_sel_wire
add wave -noupdate /MIPS_TB/DUV/UART_Tx_inst/Tx_Done_flag
add wave -noupdate /MIPS_TB/DUV/UART_Tx_inst/Tx_Transmit
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {7850 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 387
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {4764 ns} {8736 ns}
