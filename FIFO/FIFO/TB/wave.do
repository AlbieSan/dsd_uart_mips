onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /FIFO_TB/tb_fpga_clk
add wave -noupdate /FIFO_TB/tb_reset
add wave -noupdate /FIFO_TB/tb_pop
add wave -noupdate -radix unsigned /FIFO_TB/tb_Data_input
add wave -noupdate /FIFO_TB/tb_push
add wave -noupdate -radix unsigned /FIFO_TB/tb_full
add wave -noupdate /FIFO_TB/tb_empty
add wave -noupdate -radix unsigned /FIFO_TB/tb_Data_output
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {132 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 177
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {124 ps} {372 ps}
