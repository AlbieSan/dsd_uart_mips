////////////////////////////////////////////////////////////////////
//                       Moreno Contreras    						   //
//    	                 MR_One_SHOT         	                  //
//  This Module contain the One_Shot               			      //
//                        22/10/2019			                     //
////////////////////////////////////////////////////////////////////
module ONE_SHOT
(
////////// INPUTS //////////
	input clk,
	input reset,
	input load_botton,
///////// OUTPUTS ////////
	output  shot
);

/*------------------------------------------------------------------------------------------*/
/* Parametros locales para valor de los estados                                            .*/
localparam [1:0] waiting = 'b00, shooting = 'b01, waiting_stability = 'b10;
/*------------------------------------------------------------------------------------------*/
/* Registro para el cambio de estados                                                      .*/
reg [1:0] states;
/*------------------------------------------------------------------------------------------*/
/* Registro para valor de la salida                                                        .*/
reg shot_reg;
/*------------------------------------------------------------------------------------------*/
/* Logica secuencial cambio de estados                                                     .*/
always@(posedge clk, negedge reset)
begin
	if(reset== 0)
		states <= waiting;
	else begin
		case(states)
			waiting: 
			begin
				if(load_botton == 0)
					states <= shooting;
				else
					states <= waiting;
			end
			shooting:
				states <= waiting_stability;
			waiting_stability:
			begin
				if(load_botton == 0)
					states <= waiting_stability;
				else
					states <= waiting;
			end
			default:
				states <= waiting;
		endcase
			
	end	
end
/*------------------------------------------------------------------------------------------*/
/* Logica combinacional/ valores de salida de la máquina moore                             .*/
always@(*)
begin
	case(states)
	 waiting: shot_reg           = 'b0;
	 shooting: shot_reg          = 'b1;
	 waiting_stability: shot_reg = 'b1;
	 default: shot_reg           = 'b0;
	endcase

end

assign shot = shot_reg;

endmodule 