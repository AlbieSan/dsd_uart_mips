module PUSH_COUNTER
#(
	parameter ADDRESS_width = 4,
	parameter COUNTER_SIZE = $clog2(ADDRESS_width)
)
(
	input push,
	input clk,
	input reset,
	input full,
	
	output  [COUNTER_SIZE:0] add_push
);

reg [COUNTER_SIZE:0] add_push_beta;

always@(posedge clk, negedge reset) begin
	if(reset == 0)
		add_push_beta <= {ADDRESS_width{1'b0}};
	else begin
	if(push && ~full)
	  add_push_beta <= add_push_beta + 1'b1;
	
	end

end
assign add_push = add_push_beta;
endmodule 