//Reference : File Downloaded from http://www.nandland.com
//coder: Jaime Hernandez
module UART_Rx 
#(
  parameter BAUD_RATE = 115200,
  parameter CLK		 = 10_000_000,
  parameter CLKS_PER_BIT = CLK/BAUD_RATE
 )
 
  (
	//Input_Ports
   input	clk,
   input reset,
   input Serial_data_in,
   input clear_Rx_flag,
	
	//Output Ports
   output       Rx_flag,
   output [7:0] Data_Rx
	
   );
    
 localparam IDLE         = 3'b000;
 localparam RX_START_BIT = 3'b001;
 localparam RX_DATA_BITS = 3'b010;
 localparam RX_PARITY_BIT= 3'b011;
 localparam RX_STOP_BIT  = 3'b100;

   
 
  reg reg_Rx_Data					=0;
  reg reg_Rx_flag					=0;
  reg reg_parity_bit_received	=0;
  reg reg_parity_bit_calculated	=0;
  
  reg [7:0] clk_Counter		=0;
  reg [3:0] reg_Bit_Index	=0 /* synthesis preserve */;		//8 bits
  reg [7:0] reg_Rx_Byte		=0;
  reg [2:0] State /* synthesis preserve */; 
   
//Register serial data input
  always @(posedge clk)
    begin
      reg_Rx_Data <= Serial_data_in;
    end
   
	
  //RX state machine
  always @(posedge clk or negedge reset)	begin  
	if (!reset)
		State <= IDLE;
   else begin  
      case (State)
        IDLE : begin
				clk_Counter <= 8'h00;
				reg_Bit_Index   <= 4'h0;
            
				if (clear_Rx_flag)		//Clear Rx flag
					reg_Rx_flag <= 1'b0;
				else
					reg_Rx_flag <= reg_Rx_flag;
  
            if (reg_Rx_Data == 1'b0)          // Start bit detected
              State <= RX_START_BIT;
            else
              State <= IDLE;
          end
         
        // Check middle of start bit to make sure it's still low
        RX_START_BIT:	begin
            if (clk_Counter == (CLKS_PER_BIT-1)/2) begin
                if (reg_Rx_Data == 1'b0) begin
		clk_Counter <= 8'h00;  // reset counter, found the middle
		State     <= RX_DATA_BITS;
                  end
                else
                  State <= IDLE;
              end
            else
              begin
                clk_Counter <= clk_Counter + 1'b1;
                State     <= RX_START_BIT;
              end
          end 
	   // Wait CLKS_PER_BIT-1 clock cycles to sample serial data
        RX_DATA_BITS : begin
            if (clk_Counter < CLKS_PER_BIT-1) begin
                clk_Counter <= clk_Counter + 1'b1;
                State     <= RX_DATA_BITS;
              end				    
            else begin
		clk_Counter <= 8'h00;
		reg_Rx_Byte[reg_Bit_Index] <= reg_Rx_Data;
	    if(reg_Bit_Index < 4'b0111) begin
		reg_Bit_Index <= reg_Bit_Index + 1'b1;
		State     <= RX_DATA_BITS;
		end
	    else begin
		reg_Bit_Index <= 4'b0000;
		State <= RX_PARITY_BIT;
		 end
                 end

       
                        end 
        
		  // Receive parity bit
        RX_PARITY_BIT : begin
            // Wait CLKS_PER_BIT-1 clock cycles to sample parity bit
            if (clk_Counter < CLKS_PER_BIT-1) begin
                clk_Counter <= clk_Counter + 1'b1;
                State     <= RX_PARITY_BIT;
              end
            else begin
					//logic for parity bit
					reg_parity_bit_received = reg_Rx_Data;
					reg_parity_bit_calculated = ~^reg_Rx_Byte;
					if (reg_parity_bit_received ^ reg_parity_bit_calculated)
						State	<= IDLE; //error sending the data, return to idle state
					else
						State	<= RX_STOP_BIT; //data OK						
					clk_Counter 		<= 8'h00;
              end
          end   
     
        // Receive Stop bit.  Stop bit = 1
        RX_STOP_BIT : begin
            // Wait CLKS_PER_BIT-1 clock cycles for Stop bit to finish
            if (clk_Counter < CLKS_PER_BIT-1) begin
                clk_Counter <= clk_Counter + 1'b1;
                State     <= RX_STOP_BIT;
              end
            else begin
                reg_Rx_flag <= 1'b1;
                clk_Counter 		<= 8'h00;
                State				<= IDLE;
              end
          end
   
       
        default :
          State <= IDLE;
         
      endcase
    end   
   end
	
  assign Rx_flag   = reg_Rx_flag;
  assign Data_Rx = reg_Rx_Byte;
  
			 
			 
endmodule
