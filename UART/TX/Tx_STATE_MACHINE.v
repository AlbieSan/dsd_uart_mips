/////// coder: MArio Moreno
/////// piso: Control Machine to transmit data
module Tx_STATE_MACHINE #(
  parameter BAUD_RATE = 115200,
  parameter CLK		 = 10_000_000,
  parameter CLKS_PER_BIT = CLK/BAUD_RATE, //86.8 to 87
  parameter State_Lenght = $clog2(6)
)
(
   input	clk,//from PLL
	input reset,
   input Transmit,
	input overflow_baudcounter,
	input data_flag,
	output enablebaudcount,
	output enableshift,
	output enablebitcount,
	output enableload,
	output [1:0] sel
);
  localparam IDLE        		= 3'b000;
  localparam Tx_START_BIT		= 3'b001;
  localparam TX_DATA_BITS		= 3'b010;
  localparam TX_PARITY_BIT		= 3'b011;
  localparam TX_STOP_BIT		= 3'b100;
  localparam CLEANUP				= 3'b101;
 
 reg [State_Lenght-1:0]    State,current_state; 
 reg enable_loadreg;
 reg [1:0] sel_reg;
 reg en_baud_reg;
 reg en_shif_reg;
 reg enablebitcount_reg;
 
always @(posedge clk or negedge reset) 
 begin
  	if (!reset)
		State <= IDLE;
	else
	case(State)
	 IDLE : 
	  begin
      if (Transmit == 1'b1)
			State <= Tx_START_BIT;
		else
		 State <= Tx_START_BIT;
	 end
	 Tx_START_BIT: 
	 begin
		 if(overflow_baudcounter)
			 State<=TX_DATA_BITS;
		 else
			 State<=Tx_START_BIT;
	 end
	 
	 TX_DATA_BITS: 
	 begin
	 if(data_flag)
	   State<=TX_PARITY_BIT;
	 else
		State<=TX_DATA_BITS;
	 end
	 TX_PARITY_BIT: 
	 begin
		 if(overflow_baudcounter)
			 State<=TX_STOP_BIT;
		 else
			 State<=TX_PARITY_BIT;
	 end
	 TX_STOP_BIT: 
	 begin
		 if(overflow_baudcounter)
			 State<=CLEANUP;
		 else
			 State<=TX_STOP_BIT;
	 end
	 CLEANUP: State <= IDLE;	
	endcase
 end
 
always@(posedge clk,  negedge reset)
begin
if(!reset)
    current_state <= IDLE;
else
    current_state <= State;
end

always@(*)begin
case(current_state)
	IDLE:
	begin
	enable_loadreg=0;
	sel_reg = 0;
	en_baud_reg = 0;
	en_shif_reg=0;
	enablebitcount_reg =0;
	end
	Tx_START_BIT:
	begin
	enable_loadreg  = 1;
	sel_reg         = 1;
	en_baud_reg = 1;
	en_shif_reg=0;
	enablebitcount_reg = 0;
	end
	TX_DATA_BITS:
	begin
	enable_loadreg  = 0;
	sel_reg         = 2;
	en_baud_reg = 1;
	en_shif_reg =1;
	enablebitcount_reg=1;
	end
	TX_PARITY_BIT:
	begin
	enable_loadreg  = 0;
	sel_reg         = 3;
	en_baud_reg     = 1;
	en_shif_reg 	 = 0;
	enablebitcount_reg=0;
	end
	TX_STOP_BIT:
	begin
	enable_loadreg=0;
	sel_reg = 0;
	en_baud_reg = 1;
	en_shif_reg=0;
	enablebitcount_reg=0;
	end
	CLEANUP:
	begin
	enable_loadreg=0;
	sel_reg = 0;
	en_baud_reg = 0;
	en_shif_reg=0;
	enablebitcount_reg=0;
	end
endcase 
end

assign enableload = enable_loadreg;
assign sel = sel_reg;
assign enablebaudcount = en_baud_reg;
assign enableshift = en_shif_reg;
assign enablebitcount = enablebitcount_reg;
endmodule
 