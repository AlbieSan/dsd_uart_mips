// coder:Mario Moreno
// This file is the UART_TX_top module

module UART_Tx #(
  parameter Data_Size = 8,
  parameter BAUD_RATE = 115200,
  parameter CLK		 = 10_000_000,
  parameter CLKS_PER_BIT = CLK/BAUD_RATE //86.8 to 87
)
(
   input	clk,//from PLL
	input reset,
   input [Data_Size-1:0] Serial_data_Out,
	input Transmit,
	output Data_TX
);
wire [1:0]selector_wire;
wire serial_TX_datareg;
wire enable_loadata;
wire baud_countreg;
wire ena_baudcountreg;
wire datatransmited_flag;
wire enable_shift;
wire control_shift;
wire parity_wire;
wire enable_count;
wire control_bitcount;
 

piso_msb #(
.DW(Data_Size),
.Frame_lenght(11)
) 
TXDATA(
.clk(clk),    // Clock
.rst(reset),    // asynchronous reset low active 
.enb(enable_shift),    // Enable from counter
.l_s(enable_loadata),    // load or shift
.inp(Serial_data_Out),    // data input
.parity(parity_wire),
.out(serial_TX_datareg)     // Serial output
);
Tx_STATE_MACHINE #(
   .BAUD_RATE(BAUD_RATE),
   .CLK(CLK),
   .CLKS_PER_BIT(CLKS_PER_BIT)  //86.8 to 87
)
TX_MACHINE
(
   .clk(clk),//from PLL
	.reset(reset),
   .Transmit(Transmit),
	.overflow_baudcounter(baud_countreg),
	.data_flag(datatransmited_flag),
	.enablebaudcount(ena_baudcountreg),
	.enableshift(control_shift),
	.enablebitcount(control_bitcount),
	.enableload(enable_loadata),
	.sel(selector_wire)
);
Mux_3_to_1 
MUXTransmiter
(
.A(1'b1),
.B(1'b0),
.C(serial_TX_datareg),
.D(parity_wire),
.sel(selector_wire),//fromcontrol
.Out(Data_TX)
);

counter_baudrate #(
  .BAUD_RATE(BAUD_RATE),
  .CLK(CLK),
  .MAXCNT(CLKS_PER_BIT),
  .wrd_len($clog2(CLKS_PER_BIT))
)
 Counter_Baud
(

//inputs
.clk(clk),
.rst(reset),
.enb(ena_baudcountreg),//fromcontrol

//outputs
.ovf(baud_countreg)//tocontrol
//output [wrd_len-1:0] counter
);

//counter_bits
counter_baudrate #(
  .BAUD_RATE(BAUD_RATE),
  .CLK(CLK),
  .MAXCNT(8)
)
 Counter_Bits
(

//inputs
.clk(clk),
.rst(reset),
.enb(enable_count),//fromcontrol

//outputs
.ovf(datatransmited_flag)//tocontrol
//output [wrd_len-1:0] counter
);
And_Module
ANd_to_Shift
(
.A(control_shift),//fromcontrol
.B(baud_countreg),
.Out(enable_shift)
);

And_Module
ANd_to_Bit_COunter
(
.A(control_bitcount),//fromcontrol
.B(baud_countreg),
.Out(enable_count)
);

endmodule
