// coder:Mario Moreno
// UART_TX
module UART_Tx #(
  parameter Data_Size = 8,
  parameter BAUD_RATE = 115200,
  parameter CLK		 = 10_000_000,
  parameter CLKS_PER_BIT = CLK/BAUD_RATE //86.8 to 87
)
(
   input	clk,//from PLL
	input reset,
   input [Data_Size-1:0] Serial_data_Out,
	input Transmit,
	output Data_TX
);

 



endmodule
