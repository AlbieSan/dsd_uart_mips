 module RX_STATE_MACHINE #(
  parameter BAUD_RATE = 115200,
  parameter CLK		 = 10_000_000,
  parameter CLKS_PER_BIT = CLK/BAUD_RATE
 
 )
 (
	input	clk,
	input reset,
   input Serial_data_in,
	input clear_Rx_flag,
	output B
 );
 localparam IDLE         = 3'b000;
 localparam RX_START_BIT = 3'b001;
 localparam RX_DATA_BITS = 3'b010;
 localparam RX_PARITY_BIT= 3'b011;
 localparam RX_STOP_BIT  = 3'b100;